# Run the PHP support tests with as less dependencies as possible.
# Requires php-mode to be installed from MELPA for now.

EMACS=emacs
BASE_DIR=../../..

$EMACS -q -l $BASE_DIR/cedet-devel-load.el -l $BASE_DIR/contrib/cedet-contrib-load.el -l test.el --eval "(ert t)"
