;;; test.el --- Test for PHP support -*- lexical-binding: t -*-

;;; Commentary:
;; Tests for wisent-php

;; TODO: These tests relies to much on ede-php-autoload. They should
;; test only wisent-php.

;;; Code:

(require 'package)
(package-initialize)
(require 'php-mode)
(require 'wisent-php)
(require 'ede-php-autoload-mode)
(require 'ert)

(semantic-mode 1)
(global-ede-mode 1)

(defun wisent-php-check-completions-at-point (expected-completions)
  "Assert the semantic completion at current point equals EXPECTED-COMPLETIONS."
  (let ((completions (semantic-analyze-possible-completions (point))))
    (should (= (length completions) (length expected-completions)))
    (dolist (tag completions)
      (should (member (semantic-tag-name tag)
                      expected-completions)))))

(add-hook 'php-mode-hook #'ede-php-autoload-mode)

(let* ((test-dir (file-name-directory (or load-file-name buffer-file-name)))
       (php-source-dir (expand-file-name "project/" test-dir))
       (main-file (expand-file-name "main.php" php-source-dir))
       (project (ede-php-autoload-project "Test project"
                                      :file main-file)))

  (ert-deftest wisent-php-function-parsing ()
    "Wisent is able to parse a simple PHP file with a single function."
    (let* ((tags (semanticdb-file-stream main-file))
           (main (car tags)))
      (should (= (length tags) 1))
      (should (string= (semantic-tag-name main) "main"))
      (should (equal (semantic-tag-class main) 'function))))

  (ert-deftest wisent-php-in-class-completion ()
    "Semantic is able to auto-complete class fields."
    (with-current-buffer (find-file (expand-file-name "src/OldNs/MyClass.php" php-source-dir))
      (goto-char (point-min))
      (search-forward-regexp (rx "$this->" eol))
      (wisent-php-check-completions-at-point '("myProp" "testArrow"))))

  (ert-deftest wisent-php-argument-completion ()
    "Semantic is able to auto-complete fields for annotated function arguments."
    (with-current-buffer (find-file (expand-file-name "src/NewNs/GreeterCaller.php" php-source-dir))
      (goto-char (point-min))
      (search-forward-regexp (rx "$greeter->" eol))
      (wisent-php-check-completions-at-point '("greet"))))

  (ert-deftest wisent-php-argument-completion-through-alias ()
    "Semantic is able to auto-complete fields for function
    arguments annotated with a class alias."
    (with-current-buffer (find-file (expand-file-name "src/AnotherNs/GreeterCaller.php" php-source-dir))
      (goto-char (point-min))
      (search-forward-regexp (rx "$greeter->" eol))
      (wisent-php-check-completions-at-point '("greet"))))

  (ert-deftest wisent-php-super-class-completion ()
    "Semantic is able to retrieve superclass fields for auto-completion."
    (with-current-buffer (find-file (expand-file-name "src/NewNs/SuperGreeter.php" php-source-dir))
      (goto-char (point-min))
      (search-forward-regexp (rx "parent::" eol))
      (wisent-php-check-completions-at-point '("greet"))))

  (ert-deftest wisent-php-local-variable-completion ()
    "Semantic is able to retrieve local variable field
    encapsulated in IF block for auto-completion."
    (with-current-buffer (find-file (expand-file-name "src/NewNs/GreeterCaller.php" php-source-dir))
      (goto-char (point-min))
      (search-forward-regexp (rx "$greeter2->" eol))
      (wisent-php-check-completions-at-point '("greet"))))

  (ert-deftest wisent-php-in-depth-completion ()
    "Semantic is able to auto-complete fields of types that are not directly referenced in the buffer."
    (with-current-buffer (find-file (expand-file-name "src/AnotherNs/SuperGreeterCaller.php" php-source-dir))
      (goto-char (point-min))
      (search-forward-regexp (rx "$greeter->" eol))
      (wisent-php-check-completions-at-point '("greet" "superGreet"))))

  (ert-deftest wisent-php-vendor-dir-completion ()
    "ede-php-autoload projects correctly import namespaces from vendor dir."
    (with-current-buffer (find-file (expand-file-name "src/NewNs/ThirdPartyCaller.php" php-source-dir))
      (goto-char (point-min))
      (search-forward-regexp (rx "$theClass->" eol))
      (wisent-php-check-completions-at-point '("doStuff" "doOtherStuff"))))

  (ert-deftest wisent-php-partial-use-completion ()
    "Semantic is able to auto-complete fields when there is a use
    statement not referring to a file in the name."
    (with-current-buffer (find-file (expand-file-name "src/AnotherNs/AdderCaller.php" php-source-dir))
      (goto-char (point-min))
      (search-forward-regexp (rx "$adder->" eol))
      (wisent-php-check-completions-at-point '("add"))))

  (ert-deftest ede-php-autoload-autoload-psr0 ()
    "EDE is able to retrieve a PSR0 file from the project definition."
    (with-current-buffer (find-file-noselect main-file)
      (should (string=
               (ede-php-autoload-find-class-def-file project "OldNs_MyClass")
               (expand-file-name "src/OldNs/MyClass.php" php-source-dir))))))

(provide 'cedet/contrib/tests/php/test)

;;; test.el ends here
