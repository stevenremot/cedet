<?php
namespace NewNs;

class Greeter
{
    private $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function greet()
    {
        echo "Hello " . $this->name;
    }
}
